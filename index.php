<?php include_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>WDC039-1 | PHP Basics and Selection Control Structures</title>
  <link rel="stylesheet" href="style.php" media="screen">
</head>
<body>
  <div class="container">

    <h1>Full Address</h1>
    <p>
      <?php echo getFullAddress("Philippines", "Muntinlupa", "Metro Manila", "34 Crispin St."); ?>
    </p>

    <h1>Letter-Based Grading</h1>
    <p>
      <?php echo '87 is equivalent to ' . getLetterGrade(87); ?>
    </p>
    <p>
      <?php echo '94 is equivalent to ' . getLetterGrade(94); ?>
    </p>
    <p>
      <?php echo '74 is equivalent to ' . getLetterGrade(74); ?>
    </p>

  </div>
</body>
</html>